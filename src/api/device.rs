use crate::api::bindings::*;

pub fn get_device_ids(
    _platform: cl_platform_id,
    _device_type: cl_device_type,
    _num_entries: cl_uint,
    _devices: *mut cl_device_id,
    num_devices: *mut cl_uint,
) -> Result<(), cl_int> {
    // num_devices returns the number of OpenCL devices available that match device_type. If
    // num_devices is NULL, this argument is ignored.
    if num_devices.is_null() {
        unsafe {
            *num_devices = 0;
        }
    }

    // CL_DEVICE_NOT_FOUND if no OpenCL devices that matched device_type were found
    Err(CL_DEVICE_NOT_FOUND)
}

#![allow(dead_code)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

use crate::api::platform::_cl_platform_id;

pub type cl_platform_id = *const _cl_platform_id;
// otherwise we end up with u64/u32 depending on arch
pub type size_t = usize;

pub type create_context_cb = ::core::option::Option<
    unsafe extern "C" fn(
        errinfo: *const ::std::os::raw::c_char,
        private_info: *const ::core::ffi::c_void,
        cb: size_t,
        user_data: *mut ::core::ffi::c_void,
    ),
>;
pub type program_cb = ::core::option::Option<
    unsafe extern "C" fn(program: cl_program, user_data: *mut ::core::ffi::c_void),
>;

include!(concat!(env!("OUT_DIR"), "/cl-bindings.rs"));

#![allow(non_snake_case)]

use crate::api::bindings::*;
use crate::api::device::*;
use crate::api::platform::*;
use std::ffi::CStr;
use std::ptr;

pub const DISPATCH: cl_icd_dispatch = cl_icd_dispatch {
    clGetPlatformIDs: Some(cl_get_platform_ids),
    clGetPlatformInfo: Some(cl_get_platform_info),
    clGetDeviceIDs: Some(cl_get_device_ids),
    clGetDeviceInfo: None,
    clCreateContext: None,
    clCreateContextFromType: Some(cl_create_context_from_type),
    clRetainContext: None,
    clReleaseContext: None,
    clGetContextInfo: None,
    clCreateCommandQueue: None,
    clRetainCommandQueue: None,
    clReleaseCommandQueue: None,
    clGetCommandQueueInfo: None,
    clSetCommandQueueProperty: None,
    clCreateBuffer: None,
    clCreateImage2D: None,
    clCreateImage3D: None,
    clRetainMemObject: None,
    clReleaseMemObject: None,
    clGetSupportedImageFormats: None,
    clGetMemObjectInfo: None,
    clGetImageInfo: None,
    clCreateSampler: None,
    clRetainSampler: None,
    clReleaseSampler: None,
    clGetSamplerInfo: None,
    clCreateProgramWithSource: None,
    clCreateProgramWithBinary: None,
    clRetainProgram: None,
    clReleaseProgram: None,
    clBuildProgram: None,
    clUnloadCompiler: None,
    clGetProgramInfo: None,
    clGetProgramBuildInfo: None,
    clCreateKernel: None,
    clCreateKernelsInProgram: None,
    clRetainKernel: None,
    clReleaseKernel: None,
    clSetKernelArg: None,
    clGetKernelInfo: None,
    clGetKernelWorkGroupInfo: None,
    clWaitForEvents: None,
    clGetEventInfo: None,
    clRetainEvent: None,
    clReleaseEvent: None,
    clGetEventProfilingInfo: None,
    clFlush: None,
    clFinish: None,
    clEnqueueReadBuffer: None,
    clEnqueueWriteBuffer: None,
    clEnqueueCopyBuffer: None,
    clEnqueueReadImage: None,
    clEnqueueWriteImage: None,
    clEnqueueCopyImage: None,
    clEnqueueCopyImageToBuffer: None,
    clEnqueueCopyBufferToImage: None,
    clEnqueueMapBuffer: None,
    clEnqueueMapImage: None,
    clEnqueueUnmapMemObject: None,
    clEnqueueNDRangeKernel: None,
    clEnqueueTask: None,
    clEnqueueNativeKernel: None,
    clEnqueueMarker: None,
    clEnqueueWaitForEvents: None,
    clEnqueueBarrier: None,
    clGetExtensionFunctionAddress: Some(cl_get_extension_function_address),
    clCreateFromGLBuffer: None,
    clCreateFromGLTexture2D: None,
    clCreateFromGLTexture3D: None,
    clCreateFromGLRenderbuffer: None,
    clGetGLObjectInfo: None,
    clGetGLTextureInfo: None,
    clEnqueueAcquireGLObjects: None,
    clEnqueueReleaseGLObjects: None,
    clGetGLContextInfoKHR: None,
    clGetDeviceIDsFromD3D10KHR: ptr::null_mut(),
    clCreateFromD3D10BufferKHR: ptr::null_mut(),
    clCreateFromD3D10Texture2DKHR: ptr::null_mut(),
    clCreateFromD3D10Texture3DKHR: ptr::null_mut(),
    clEnqueueAcquireD3D10ObjectsKHR: ptr::null_mut(),
    clEnqueueReleaseD3D10ObjectsKHR: ptr::null_mut(),
    clSetEventCallback: None,
    clCreateSubBuffer: None,
    clSetMemObjectDestructorCallback: None,
    clCreateUserEvent: None,
    clSetUserEventStatus: None,
    clEnqueueReadBufferRect: None,
    clEnqueueWriteBufferRect: None,
    clEnqueueCopyBufferRect: None,
    clCreateSubDevicesEXT: None,
    clRetainDeviceEXT: None,
    clReleaseDeviceEXT: None,
    clCreateEventFromGLsyncKHR: None,
    clCreateSubDevices: None,
    clRetainDevice: None,
    clReleaseDevice: None,
    clCreateImage: None,
    clCreateProgramWithBuiltInKernels: None,
    clCompileProgram: None,
    clLinkProgram: None,
    clUnloadPlatformCompiler: None,
    clGetKernelArgInfo: None,
    clEnqueueFillBuffer: None,
    clEnqueueFillImage: None,
    clEnqueueMigrateMemObjects: None,
    clEnqueueMarkerWithWaitList: None,
    clEnqueueBarrierWithWaitList: None,
    clGetExtensionFunctionAddressForPlatform: None,
    clCreateFromGLTexture: None,
    clGetDeviceIDsFromD3D11KHR: ptr::null_mut(),
    clCreateFromD3D11BufferKHR: ptr::null_mut(),
    clCreateFromD3D11Texture2DKHR: ptr::null_mut(),
    clCreateFromD3D11Texture3DKHR: ptr::null_mut(),
    clCreateFromDX9MediaSurfaceKHR: ptr::null_mut(),
    clEnqueueAcquireD3D11ObjectsKHR: ptr::null_mut(),
    clEnqueueReleaseD3D11ObjectsKHR: ptr::null_mut(),
    clGetDeviceIDsFromDX9MediaAdapterKHR: ptr::null_mut(),
    clEnqueueAcquireDX9MediaSurfacesKHR: ptr::null_mut(),
    clEnqueueReleaseDX9MediaSurfacesKHR: ptr::null_mut(),
    clCreateFromEGLImageKHR: None,
    clEnqueueAcquireEGLObjectsKHR: None,
    clEnqueueReleaseEGLObjectsKHR: None,
    clCreateEventFromEGLSyncKHR: None,
    clCreateCommandQueueWithProperties: None,
    clCreatePipe: None,
    clGetPipeInfo: None,
    clSVMAlloc: None,
    clSVMFree: None,
    clEnqueueSVMFree: None,
    clEnqueueSVMMemcpy: None,
    clEnqueueSVMMemFill: None,
    clEnqueueSVMMap: None,
    clEnqueueSVMUnmap: None,
    clCreateSamplerWithProperties: None,
    clSetKernelArgSVMPointer: None,
    clSetKernelExecInfo: None,
    clGetKernelSubGroupInfoKHR: None,
    clCloneKernel: None,
    clCreateProgramWithIL: None,
    clEnqueueSVMMigrateMem: None,
    clGetDeviceAndHostTimer: None,
    clGetHostTimer: None,
    clGetKernelSubGroupInfo: None,
    clSetDefaultDeviceCommandQueue: None,
    clSetProgramReleaseCallback: None,
    clSetProgramSpecializationConstant: None,
    clCreateBufferWithProperties: None,
    clCreateImageWithProperties: None,
};

// We need those functions exported

#[no_mangle]
extern "C" fn clGetPlatformInfo(
    platform: cl_platform_id,
    param_name: cl_platform_info,
    param_value_size: size_t,
    param_value: *mut ::core::ffi::c_void,
    param_value_size_ret: *mut size_t,
) -> cl_int {
    return cl_get_platform_info(
        platform,
        param_name,
        param_value_size,
        param_value,
        param_value_size_ret,
    );
}

#[no_mangle]
extern "C" fn clGetExtensionFunctionAddress(
    function_name: *const ::std::os::raw::c_char,
) -> *mut ::core::ffi::c_void {
    return cl_get_extension_function_address(function_name);
}

#[no_mangle]
extern "C" fn clIcdGetPlatformIDsKHR(
    num_entries: cl_uint,
    platforms: *mut cl_platform_id,
    num_platforms: *mut cl_uint,
) -> cl_int {
    return cl_icd_get_platform_ids_khr(num_entries, platforms, num_platforms);
}

// extern "C" function stubs in ICD and extension order

extern "C" fn cl_get_platform_ids(
    num_entries: cl_uint,
    platforms: *mut cl_platform_id,
    num_platforms: *mut cl_uint,
) -> cl_int {
    match get_platform_ids(num_entries, platforms, num_platforms) {
        Ok(_) => CL_SUCCESS as cl_int,
        Err(e) => e,
    }
}

extern "C" fn cl_get_platform_info(
    platform: cl_platform_id,
    param_name: cl_platform_info,
    param_value_size: size_t,
    param_value: *mut ::core::ffi::c_void,
    param_value_size_ret: *mut size_t,
) -> cl_int {
    match get_platform_info(
        platform,
        param_name,
        param_value_size,
        param_value,
        param_value_size_ret,
    ) {
        Ok(_) => CL_SUCCESS as cl_int,
        Err(e) => e,
    }
}

extern "C" fn cl_get_device_ids(
    platform: cl_platform_id,
    device_type: cl_device_type,
    num_entries: cl_uint,
    devices: *mut cl_device_id,
    num_devices: *mut cl_uint,
) -> cl_int {
    match get_device_ids(platform, device_type, num_entries, devices, num_devices) {
        Ok(_) => CL_SUCCESS as cl_int,
        Err(e) => e,
    }
}

extern "C" fn cl_create_context_from_type(
    _properties: *const cl_context_properties,
    _device_type: cl_device_type,
    _pfn_notify: create_context_cb,
    _user_data: *mut ::core::ffi::c_void,
    _errcode_ret: *mut cl_int,
) -> cl_context {
    ptr::null_mut()
}

extern "C" fn cl_get_extension_function_address(
    function_name: *const ::std::os::raw::c_char,
) -> *mut ::core::ffi::c_void {
    if function_name.is_null() {
        return ptr::null_mut();
    }
    match unsafe { CStr::from_ptr(function_name) }.to_str().unwrap() {
        "clGetPlatformInfo" => cl_get_platform_info as *mut std::ffi::c_void,
        "clIcdGetPlatformIDsKHR" => cl_icd_get_platform_ids_khr as *mut std::ffi::c_void,
        _ => ptr::null_mut(),
    }
}

// cl_khr_icd
extern "C" fn cl_icd_get_platform_ids_khr(
    num_entries: cl_uint,
    platforms: *mut cl_platform_id,
    num_platforms: *mut cl_uint,
) -> cl_int {
    match get_platform_ids(num_entries, platforms, num_platforms) {
        Ok(_) => CL_SUCCESS as cl_int,
        Err(e) => e,
    }
}

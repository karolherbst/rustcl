use crate::api::bindings::*;

use std::ffi::CString;

pub fn mk_cl_version(major: u32, minor: u32, patch: u32) -> cl_version {
    major & 0x3ff << 22 |
    minor & 0x3ff << 12 |
    patch & 0xfff
}

pub fn str_as_nul_vec(s: &str) -> Result<Vec<u8>, cl_int> {
    Ok(CString::new(s)
        .or(Err(CL_OUT_OF_HOST_MEMORY))?
        .into_bytes_with_nul())
}

#[test]
fn test_nul_string() {
    let r = str_as_nul_vec("d");
    assert!(r.is_ok());
    let s = r.unwrap();
    assert_eq!(s[0], 'd' as u8);
    assert_eq!(s[1], '\0' as u8);
}

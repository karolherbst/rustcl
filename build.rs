extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=cl-wrapper.h");

    let bindings = bindgen::builder()
        .header("cl-wrapper.h")
        .clang_arg("-DCL_TARGET_OPENCL_VERSION=300")
        .no_convert_floats()
        .ignore_functions()
        .blacklist_type("cl_platform_id")
        .blacklist_type("_cl_platform_id")
        .blacklist_type("size_t")
        .use_core()
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .unwrap();

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("cl-bindings.rs"))
        .expect("Couldn't write bindings!");
}
